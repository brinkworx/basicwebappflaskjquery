from itertools import chain

__pragma__('skip')  # pylint: disable=E0602
document = window = console = io = location = Math = Date = 0  # Prevent complaints by optional static checker
S = lambda: 0
__pragma__('noskip')  # pylint: disable=E0602
__pragma__('alias', 'S', '$')  # pylint: disable=E0602


class SolarSystem:
    planets = [list(chain(planet, (index + 1,))) for index, planet in enumerate((
        ('Mercury', 'hot', 2240),
        ('Venus', 'sulphurous', 6052),
        ('Earth', 'fertile', 6378),
        ('Mars', 'reddish', 3397),
        ('Jupiter', 'stormy', 71492),
        ('Saturn', 'ringed', 60268),
        ('Uranus', 'cold', 25559),
        ('Neptune', 'very cold', 24766)
    ))]

    lines = (
        '{} is a {} planet',
        'The radius of {} is {} km',
        '{} is planet nr. {} counting from the sun'
    )

    def __init__(self):
        self.planet = None
        self.lineIndex = 0

    def greet(self):
        self.planet = self.planets[int(Math.random() * len(self.planets))]
        document.getElementById('greet').innerHTML = 'Hello {}'.format(self.planet[0])
        self.explain()

    def explain(self):
        document.getElementById('explain').innerHTML = (
            self.lines[self.lineIndex].format(self.planet[0], self.planet[self.lineIndex + 1])
        )
        self.lineIndex = (self.lineIndex + 1) % 3


solarSystem = SolarSystem()


class SolarSocket:
    def __init__(self):
        self.planet = None
        self.lineIndex = 0
        self.socket = io.connect('http://' + document.domain + ':' + location.port)
        self.socket.on('connect', self.onConnect)
        self.socket.on('disconnect', self.onDisconnect)
        self.socket.on('server_click', self.onResponseClick)
        self.socket.on('server_auto', self.onResponseServerAuto)

    def onConnect(self):
        console.log('socketIO connected')
        S('#sock_click_me').prop('disabled', False)

    def onDisconnect(self):
        console.log('socketIO disconnected')
        S('#sock_click_me').prop('disabled', True)

    def onResponseServerAuto(self, msg):
        S('#socketresponse_server').text('Received #' + msg.count + ': ' + msg.data)

    def onResponseClick(self, msg):
        console.log('socketIO click')
        S('#socketresponse_click').append('<br>' + S('<div/>').text('Received #' + msg.count + ': ' + msg.data).html())

    def greet(self):
        self.socket.emit('client_click', {'data': 'Button clicked'})


solarSystem = SolarSystem()
solarSocket = SolarSocket()


def elm(elm_type, Props=None, Css=None):
    s_control = S(f'<{elm_type}/>')
    if Props is not None:
        s_control.attr(Props)
    if Css is not None:
        s_control.css(Css)
    return s_control


DIV = lambda content=None, Props=None, Css=None: elm('div', Props, Css) if content is None else elm('div', Props, Css).html(content)
H1 = lambda content, Props=None, Css=None: elm('h1', Props, Css).html(content)
H2 = lambda content, Props=None, Css=None: elm('h2', Props, Css).html(content)
H3 = lambda content, Props=None, Css=None: elm('h3', Props, Css).html(content)
H4 = lambda content, Props=None, Css=None: elm('h4', Props, Css).html(content)
H5 = lambda content, Props=None, Css=None: elm('h5', Props, Css).html(content)
H6 = lambda content, Props=None, Css=None: elm('h6', Props, Css).html(content)
P = lambda content, Props=None, Css=None: elm('p', Props, Css).html(content)
OL = lambda content, Props=None, Css=None: elm('ol', Props, Css).html(content)
UL = lambda content=None, Props=None, Css=None: elm('ul', Props, Css) if content is None else elm('ul', Props, Css).html(content)
LI = lambda content=None, Props=None, Css=None: elm('li', Props, Css) if content is None else elm('li', Props, Css).html(content)


def A(Href, content=None, Props=None, Css=None):
    if Props is None:
        Props = {}
    Props['href'] = Href
    Elem = elm('a', Props, Css)
    if content is None:
        return Elem
    return Elem.html(content)


BR = lambda: S('<BR>')


def Tabs(content_dictionary):
    """
    param content_dictionary is
        dict {
        fragment:{'label':"text", 'content':"text"}
        }
    """
    parent_div = DIV(None, {'id': 'tabs'})
    ul_elem = UL()
    parent_div.append(ul_elem)
    for key in content_dictionary.keys():
        ul_elem.append(LI(A('#' + key, content_dictionary[key]['label'])))
        parent_div.append(DIV(content_dictionary[key]['content'], {'id': key}))
    return parent_div


def start():
    console.log("Html begin")
    oldDemo = S('#demo')
    oldHelp = S('#help')
    oldSock = S('#sock')
    content_dictionary = {
        'fragment-1': {'label': "BasicWebAppFlaskJQuery", 'content': oldHelp},
        'fragment-2': {'label': "JQuery Demo", 'content': oldDemo},
        'fragment-3': {'label': "SocketIO Demo", 'content': oldSock},
    }
    S('body').html(Tabs(content_dictionary)).tabs()
    S('#help h2').remove()
    S('#demo h2').remove()
    S('#sock h2').remove()
    S('#demo').attr('style', '')  # Clear the style hiding the div
    S('#sock').attr('style', '')  # Clear the style hiding the div
    console.log("Html done")

# to dev/test
# watchmedo shell-command -p "*hello.*" -c "echo compiling & transcrypt -b -m -n -a hello.py" -W
