#!/bin/bash
source ./pyenv/bin/activate
cd webapp
echo "compiling & transcrypt -b -m -n -a index.py"
watchmedo shell-command -p "*index.*" -c 'echo "compiling" & transcrypt -b -m -n -a index.py' -W
deactivate