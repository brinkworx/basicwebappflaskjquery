@echo off
virtualenv --no-download pyenv
call .\pyenv\Scripts\activate
pip install -r requirements.txt
call deactivate
