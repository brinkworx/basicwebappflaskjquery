## BasicWebAppFlaskJQuery

### Need Python3.6 for windows
For windows download python3.6 from python.org

For linux Ubuntu16.4 follow these steps:

`sudo add-apt-repository ppa:deadsnakes/ppa`

`sudo apt-get update`

`sudo apt-get install python3.6`

### Basic single page web application done with flask and jQuery (and friends).

To be clear this is a first attempt at web-apps and my goal is to progress slowly creating a light framework I can use.

### Quick Setup

Install Visual Studio Code

### Install Git
Install SourceTree and clone this repository.

Or Git Clone it with:

`git clone https://bitbucket.org/brinkworx/basicwebappflaskjquery.git BasicWebApp`

### Setup Environment
Change to the cloned folder:

`cd BasicWebApp`

Credential Caching (if required under linux)

`git config --global credential.helper 'cache --timeout=3600'`

From inside BasicWebApp folder execute:

Both : `python -m pip install --user virtualenv`

windows : `make.venv`

linux : `./make.venv.sh`

### To Develop
Open Folder **BasicWebApp** in Visual Studio Code and run **BasicWebApp.py** to test that it is working.

### To Test in a terminal

From inside BasicWebApp folder run  **server** with:

windows : `server`

linux : `./server.sh`

### Notes
Included in the src folder is the test JQuery files I use for development purposes, by default these will be used. To change to prodiction mode do the following:

Open **config.json** and change  the line with `"debug":true,` to `"debug":false,` and reload the page, the minified version of JQuery will now be loaded from Google's CDN. While you are at it also change the `"secretkey"` value to something else, I used a 24 character random string but this can be improved with some additional coding.


### Development and Transpiling
Change src/config.json "debug" to true

In one terminal run the server

windows : `server`

linux : `./server.sh`

In another terminal run the transpiler

windows : `transpile`

linux : `./transpile.sh`
