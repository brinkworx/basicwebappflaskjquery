import os
import gzip
import json
import inspect


from flask import Flask, make_response
from flask_sessionstore import Session
from flask_login import LoginManager, UserMixin, current_user, login_user, logout_user
from flask_socketio import SocketIO, emit, send


""" Base Functionality
====================================================================="""


def root_dir():
    ''' Get the root directory of the python applications. '''
    curr_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
    lib_dir = os.path.dirname(curr_file)
    root_path = os.path.dirname(lib_dir)
    return root_path


"""------------------------------------------------------------------"""


config = None  # { 'debug':False, "secretkey":"!secret-key#123", "io-channel":"test"}


def loadCfg():
    ''' Load configuration from root/src/config.json '''
    global config
    if config is None:
        try:
            root_path = root_dir()
            src = os.path.join(root_path, 'src', 'config.json')
            with open(src, 'r') as fp:
                content = fp.read()
                fp.close()
            config = json.loads(content)
        except IOError as exc:
            print(str(exc))
    return config


"""------------------------------------------------------------------"""


""" Basic Caching System
====================================================================="""
__magic_max_entry_size = 2**10  # 1 Kilobyte
__magic_max_cache_size = 2**24  # 16 Megabyte
__magic_cache_size = 0
__magic_cache = {}
__magic_key_buffer = []


def AddToCache(key, data):
    ''' add data by key - will not be added if size exceed __magic_max_entry_size'''
    global __magic_cache_size
    global __magic_cache
    global __magic_key_buffer
    len_data = len(data)
    success = False
    if key in __magic_cache.keys():
        __magic_cache_size -= __magic_cache[key]['size']
        idx = __magic_key_buffer.index(key)
        del __magic_key_buffer[idx]
        if len_data <= __magic_max_entry_size:
            __magic_cache_size += len_data
            __magic_cache[key] = {'size': len_data, 'data': data}
            __magic_key_buffer.append(key)
            success = True
        else:
            del __magic_cache[key]
    elif len_data <= __magic_max_entry_size:
        __magic_cache[key] = {'size': len_data, 'data': data}
        __magic_key_buffer.append(key)
        success = True
    if success:
        while __magic_cache_size > __magic_max_cache_size:
            key = __magic_key_buffer[0]
            del __magic_key_buffer[0]
            __magic_cache_size -= __magic_cache[key]['size']
            del __magic_cache[key]


def GetFromCache(key):
    ''' Retrieve data by key '''
    global __magic_cache
    data = None
    if key in __magic_cache.keys():
        data = __magic_cache[key]['data']
    return data


""" Default Configuration Setup
====================================================================="""
BasicFlaskApp = Flask("BasicWebApp")
BasicFlaskApp.config['DEBUG'] = loadCfg()['debug']
BasicFlaskApp.config['SESSION_TYPE'] = 'filesystem'
BasicFlaskApp.config['SECRET_KEY'] = loadCfg()['secretkey']
BasicFlaskApp.config['SESSION_FILE_DIR'] = os.path.join(root_dir(), 'sessions')
BasicFlaskApp.debug = BasicFlaskApp.config['DEBUG']
login = LoginManager(BasicFlaskApp)
Session(BasicFlaskApp)
BasicFlaskApp_socketio = SocketIO(BasicFlaskApp, manage_session=False)


def setAppName(name):
    ''' Get the import directory of the python applications. '''
    global BasicFlaskApp
    BasicFlaskApp.import_name = name


def StartBasicFlaskApp(host=None, port=None, **kwargs):
    """
        :param host: the hostname to listen on. Set this to ``'0.0.0.0'`` to
                     have the server available externally as well. Defaults to
                     ``'127.0.0.1'``.
        :param port: the port of the webserver. Defaults to ``5000`` or the
                     port defined in the ``SERVER_NAME`` config variable if
                     present.
    """
    global BasicFlaskApp
    global BasicFlaskApp_socketio

    BasicFlaskApp_socketio.run(BasicFlaskApp, host, port, debug=loadCfg()['debug'], use_reloader=loadCfg()['debug'], **kwargs)


"""------------------------------------------------------------------"""


""" Content Loading and Packaging
====================================================================="""


def get_file(filename, basedir='src'):  # pragma: no cover
    ''' Returns the binary content of an existing file or None '''
    try:
        src = os.path.join(root_dir(), basedir, filename)
        # Figure out how flask returns static files
        # Tried:
        # - render_template
        # - send_file
        # This should not be so non-obvious
        key = 'FD_' + filename
        content = None
        if not loadCfg()['debug']:
            content = GetFromCache(key)
        if content is None:
            with open(src, 'rb') as fp:
                content = fp.read()
                fp.close()
            if not loadCfg()['debug']:
                AddToCache(key, content)
        return content
    except IOError as exc:
        print(str(exc))
        return None


"""------------------------------------------------------------------"""


def get_binary_response(request, binfile, mime, content, respCode):  # pylint: disable=W0613
    ''' Package a binary file, usualy images or downloads. '''
    response = make_response(content, respCode)
    response.headers.set('Server', 'ToDo Server')
    response.headers.set('Content-Type', mime)
    response.headers.set('Content-Disposition', 'attachment', filename=binfile)
    return response


"""------------------------------------------------------------------"""


def get_gzip_content_reponse(request, binfile, mine, content, respCode, sourcemap=None):  # pylint: disable=W0613
    ''' Compress and package text content. '''
    key = 'GZ_' + binfile
    gzip_content = None
    if not loadCfg()['debug']:
        gzip_content = GetFromCache(key)
    if gzip_content is None:
        gzip_content = gzip.compress(content, 9)
        if not loadCfg()['debug']:
            AddToCache(key, gzip_content)
    response = make_response(gzip_content, respCode)
    response.headers.set('Content-Encoding', 'gzip')
    response.headers.set('Vary', 'Accept-Encoding')
    response.headers.set('Server', 'ToDo Server')
    response.headers.set('Content-Disposition', 'inline')
    response.headers.set('Content-Type', mine)
    if sourcemap is not None:
        response.headers.set('X-SourceMap', sourcemap)
    return response


"""------------------------------------------------------------------"""


def get_content_response(request, binfile, mime, content, respCode, sourcemap=None):
    ''' Package text content, defer to compresed option if it is
        supported by the browser. '''
    accept_encoding = request.headers.get('Accept-Encoding', '')
    if 'gzip' in accept_encoding.lower():
        return get_gzip_content_reponse(request, binfile, mime, content, respCode, sourcemap)
    response = make_response(content, respCode)
    response.headers.set('Server', 'ToDo Server')
    response.headers.set('Content-Disposition', 'inline')
    response.headers.set('Content-Type', mime)
    if sourcemap is not None:
        response.headers.set('X-SourceMap', sourcemap)
    return response


"""------------------------------------------------------------------"""


def get_null_content(request):  # pylint: disable=W0613
    ''' Package valid return of 'None' content '''
    return '', 204


"""------------------------------------------------------------------"""
