from flask import make_response
from jinja2 import Template
from applibs.apputils import BasicFlaskApp, root_dir, loadCfg, get_file, get_content_response, get_binary_response, get_null_content

"""
    HTTP Error Handlers
====================================================================="""
@BasicFlaskApp.errorhandler(401)
def page_not_authed(e):
    print(e.description)
    source = get_file("401.html")
    template = Template(source.decode("utf-8"))
    content = bytes(template.render(debug=(loadCfg()['debug'])), "utf-8")
    response = make_response(content, 401)
    response.headers.set('Server', 'ToDo Server')
    response.headers.set('Content-Disposition', 'inline')
    response.headers.set('Content-Type', 'text/html')
    return response


@BasicFlaskApp.errorhandler(404)
def page_not_found(e):
    print(e.description)
    if isinstance(e.description, dict):
        file_name = e.description['filename']
    else:
        file_name = None
    source = get_file("404.html")
    source = source.decode("utf-8")
    template = Template(source)
    content = bytes(template.render(debug=(loadCfg()['debug']), filename=file_name), "utf-8")
    response = make_response(content, 404)
    response.headers.set('Server', 'ToDo Server')
    response.headers.set('Content-Disposition', 'inline')
    response.headers.set('Content-Type', 'text/html')
    return response
