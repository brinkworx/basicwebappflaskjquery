#!/bin/bash
/usr/bin/python3.6 -m virtualenv --no-download -p python3.6 pyenv
source pyenv/bin/activate
which pip
pip install --upgrade pip
pip install -r requirements.txt
cd pyenv
ln -s bin Scripts
deactivate
