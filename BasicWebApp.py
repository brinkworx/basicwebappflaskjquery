import os
import gzip
import json

import threading

from flask import Flask, session, abort, jsonify, Response
from flask import request, make_response
from flask import render_template_string, render_template
from flask_sessionstore import Session
from jinja2 import Template

from flask_login import LoginManager, UserMixin, current_user, login_user, logout_user
from flask_socketio import SocketIO, emit, send

from applibs.apputils import BasicFlaskApp, BasicFlaskApp_socketio
from applibs.apputils import root_dir, loadCfg, get_file, get_content_response, get_binary_response, get_null_content, AddToCache, GetFromCache


import applibs.errorhandlers

"""
    HTTP Serving Src directory
        Directory include special files
====================================================================="""


@BasicFlaskApp.route("/")
@BasicFlaskApp.route("/index.html")
def index():
    file_name = "index.html"
    key = 'TM_' + file_name
    content = None
    if not loadCfg()['debug']:
        content = GetFromCache(key)
    if content is None:
        source = get_file(file_name, basedir='webapp')
        if source is None:
            source = get_file(file_name)
        if source is not None:
            template = Template(source.decode("utf-8"))
            content = bytes(template.render(debug=(loadCfg()['debug'])), "utf-8")
            if loadCfg()['debug']:
                AddToCache(key, content)
            return get_content_response(request, "index.html", 'text/html', content, 200)
        else:
            return abort(404, {'filename': file_name})
    else:
        return get_content_response(request, "index.html", 'text/html', content, 200)


@BasicFlaskApp.route("/favicon.ico")
def favicon():
    ''' Serving the favicon '''
    file_name = 'favicon.ico'
    key = 'ICO_' + file_name
    content = None
    if not loadCfg()['debug']:
        content = GetFromCache(key)
    if content is None:
        content = get_file(file_name, basedir='webapp')
        if (content is not None) and loadCfg()['debug']:
            AddToCache(key, content)
    if content is None:
        content = get_file(file_name)
        if (content is not None) and loadCfg()['debug']:
            AddToCache(key, content)
    if content is not None:
        return get_binary_response(request, file_name, 'image/x-icon', content, 200)
    return abort(404, {'filename': file_name})


@BasicFlaskApp.route("/jquery-3.3.1.js")
def jquery():
    ''' Serving the debug version of jquery '''
    file_name = 'jquery-3.3.1.js'
    key = 'JS_' + file_name
    content = GetFromCache(key)
    if content is None:
        content = get_file(file_name)
        AddToCache(key, content)
    if content is not None:
        return get_content_response(request, file_name, 'application/json', content, 200, 'jquery-3.3.1.min.map')
    return abort(404, {'filename': file_name})


@BasicFlaskApp.route("/jquery-3.3.1.min.map")
def jquery_map():
    ''' Serving the map file of the jquery debug version '''
    file_name = 'jquery-3.3.1.min.map'
    key = 'JM_' + file_name
    content = GetFromCache(key)
    if content is None:
        content = get_file(file_name)
        AddToCache(key, content)
    if content is not None:
        return get_content_response(request, file_name, 'application/octet-stream', content, 200)
    return abort(404, {'filename': file_name})


"""------------------------------------------------------------------"""

""" C L A S S E S
====================================================================="""


class User(UserMixin, object):
    def __init__(self, userid=None):
        self.id = userid


"""------------------------------------------------------------------"""

"""
    HTTP Session Serving - For debug purposes
====================================================================="""


@BasicFlaskApp.route('/session', methods=['GET', 'POST'])
def session_access():
    if request.method == 'GET':
        return jsonify({
            'session': session.get('value', ''),
            'user': current_user.id if current_user.is_authenticated else 'anonymous'
        })
    data = request.get_json()
    if 'session' in data:
        session['value'] = data['session']
    elif 'user' in data:
        if data['user']:
            login_user(User(data['user']))
        else:
            logout_user()
    return get_null_content(request)


"""------------------------------------------------------------------"""

""" Everything in WebApp directory are Browser-size files
  -- These include any file types
====================================================================="""


@BasicFlaskApp.route('/', defaults={'path': ''})
@BasicFlaskApp.route('/<path:path>')
def get_resource(path):  # pragma: no cover
    file_name = path
    key = 'APP_' + file_name
    mimetypes = {
        ".css": "text/css",
        ".html": "text/html",
        ".js": "application/javascript",
    }
    # complete_path = os.path.join(root_dir(), 'webapp', path)
    ext = os.path.splitext(path)[1]
    mimetype = mimetypes.get(ext, "text/html")
    content = None
    if not loadCfg()['debug']:
        content = GetFromCache(key)
    if content is None:
        content = get_file(file_name, 'webapp')
        if not loadCfg()['debug']:
            AddToCache(key, content)
    if content is not None:
        if (mimetype in mimetypes) or mimetype.startswith('text/'):
            return get_content_response(request, file_name, mimetype, content, 200)
        return get_binary_response(request, file_name, mimetype, content, 200)
    else:
        return abort(404, {'filename': file_name})
    # return Response(content, mimetype=mimetype)


"""------------------------------------------------------------------"""

"""
    SocketIO
====================================================================="""


thread = None
thread_lock = threading.Lock()


def background_thread():
    """Threaded background Function."""
    count = 0
    while True:
        BasicFlaskApp_socketio.sleep(1)
        count += 1
        # Send this message to all users
        BasicFlaskApp_socketio.emit('server_auto', {'data': 'Server generated event', 'count': count})


@BasicFlaskApp_socketio.on('connect')
def handle_sock_connect_message():
    """ When a new SocketIO connection is made.
        It will initiate the background thread the first time it is called."""
    global thread
    global thread_lock
    with thread_lock:
        if thread is None:
            thread = BasicFlaskApp_socketio.start_background_task(target=background_thread)
    if loadCfg()['debug']:
        print('=' * 80)
        print(f'socketio: connection {request.sid}')
        print('=' * 80)


@BasicFlaskApp_socketio.on('disconnect')
def handle_sock_disconnect_message():
    """ When a SocketIO disconnection is detected."""
    if loadCfg()['debug']:
        print('=' * 80)
        print(f'socketio: disconnection {request.sid}')
        print('=' * 80)


# Note:
# @BasicFlaskApp_socketio.on('client_click', namespace='/')
# is equivelent to the following and means that the user "Loby" room was specified
# Also notice that each user automaticaly have their own private room allocated to their socket id (sid)
@BasicFlaskApp_socketio.on('client_click')
def handle_sock_client_click(message):
    """ Handle the button click event."""
    if loadCfg()['debug']:
        print(f'received event: {str(message)}')
    session['receive_count'] = session.get('receive_count', 0) + 1
    # Tell the user the server received the click, notice the room set to the users socket id
    BasicFlaskApp_socketio.emit('server_click', {'data': f'Server acknowledge Your "' + message['data'] + '"', 'count': session['receive_count']}, room=request.sid)
    # Now tell the oter users about it as well, notice the skip_id is set to the users socket id
    BasicFlaskApp_socketio.emit('server_click', {'data': 'Server acknowledged Other Users "' + message['data'] + '"', 'count': session['receive_count']}, skip_sid=request.sid)


"""
    M A I N
====================================================================="""


if __name__ == '__main__':
    from applibs.apputils import StartBasicFlaskApp
    StartBasicFlaskApp()
"""------------------------------------------------------------------"""
